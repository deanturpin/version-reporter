#!/bin/bash

echo -n "kernel "
uname -a

# bash --version
# cat /etc/os-release
# g++ --version

echo -n "bash: $(bash --version) - "
[[ $(bash --version) =~ 5\.0\.17 ]] && echo OK || (echo mismatch && exit 1)

echo -n "gcc: $(g++-10 --version) -"
[[ $(g++-10 --version) =~ 10\.2\.0 ]] && echo OK || (echo mismatch && exit 1)

echo -n "clang: $(clang++-11 --version) - "
[[ $(clang++-11 --version) =~ 11\.0\.0 ]] && echo OK || (echo mismatch && exit 1)
